# Architecture #
- Architecture follows MVVM guidelines with DataBinding and ViewStates
- Dependency Injection is achieved with Dagger 2 Android Injection
- DataBase is managed via ROOM ORM
- Threading and asynchronous execution is made inside Coroutines

## Package Structure ##
For simplicity, the project is organized in **Feature Packages** instead of modules. Feature packages are grouped together in layers. Example, **Home Page** feature has 3 packages called **home** one in Api Layer (`api` package), one in DB Layer (`db` package) and another one on the presentation layer (`ui` package). They represent different parts of layered architecture.

### common package ###
Contains all necessary classes used across the app: `App` class that extends `Application`, base DI component and main DI module.

### api package ###
Represents Api Layer and contains following classes:

- `home/ApiRepository` part of **Home Feature**, ApiRepository is responsible of retrieving all the DTOs from remote host.
- `DTOs.kt` contains all necessary data classes used for API communication.
- `ApiModule` Dagger 2 module for API communication.
- `ApiService` Retrofit 2 service Interface

### db package ###
- `home/HomeDbRepository` stores or retrieves movies from the DB. It does store the creation time for every DTO it manages so when `getMovies()` is called, it will check if the data is still valid return it to the caller. Otherwise, if data has expired, it will remove them from DB.
- `AppDatabase` and `MovieDtoDao` configuration classes and queries for ROOM


### ui.home package ###
Contains all the presentation logic:

- `HomeActivity` entry point for the App, with Grid RecyclerView and Search field.
- `HomeViewModel` associated ViewModel for the activity with the same name.
- `MoviesUseCase` business logic associated to **Home Feature**. Alternatively it can be placed in a separate `biz` package that will contain all the business logic.
- `binding/BindingExtensions` contains BindingAdapters for main screen.
- `di/HomeModule` Dagger 2 Module for *HomeFeature*
- `lists` contains all the classes necessary for main RecyclerView, including MovieModel

## Data Classes ##
All the layers are coupled together via single data flow that will be mapped from layer to layer:

- DTOs are API and Persistence layer data. In this example they are grouped together but they can be separate type of DTOs based on specific needs. For example if `MovieDTO` returned by the API needs some extra information for better caching, then there can be a mapper or a map function to transform `MovieDTO` into `MovieDbDTO` with extra fields.
- Domain data represents business logic layer data. It should not contain any extra metadata. Not strictly necessary in this particular example but in larger applications it might be convenient to pass around features.
- Models, or UiModels are classes that where mapped from Domains and may contain presentation logic metadata.


## Improvements ##
- UI tests to check view states with `MockWebServer`
- unit tests to cover UseCases, ViewModels and Repositories
