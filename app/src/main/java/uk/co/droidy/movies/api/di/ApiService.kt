package uk.co.droidy.movies.api.di

import retrofit2.http.GET
import uk.co.droidy.movies.api.dtos.ResponseDTO


interface ApiService {

    @GET("movies")
    suspend fun getMovies(): ResponseDTO

}