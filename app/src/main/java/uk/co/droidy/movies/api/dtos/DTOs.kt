package uk.co.droidy.movies.api.dtos

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.Date

class ResponseDTO(
    val data: List<MovieDTO>
)


@Entity
data class MovieDTO(
    @PrimaryKey val id: String,
    val title: String,
    val year: String,
    val genre: String,
    val poster: String,
    val creationTime: Long = System.currentTimeMillis()
)
