package uk.co.droidy.movies.api.home

import uk.co.droidy.movies.api.di.ApiService
import uk.co.droidy.movies.api.dtos.ResponseDTO
import javax.inject.Inject

class HomeApiRepository @Inject constructor(
        private val apiService: ApiService
) {

    suspend fun getHomePage(): ResponseDTO {
        return apiService.getMovies()
    }

}