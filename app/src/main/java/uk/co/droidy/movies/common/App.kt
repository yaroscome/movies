package uk.co.droidy.movies.common

import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import uk.co.droidy.movies.common.di.DaggerAppComponent
import javax.inject.Inject


class App: Application(), HasAndroidInjector {

    @Inject
    lateinit var injector: DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any> = injector

    override fun onCreate() {
        super.onCreate()

        DaggerAppComponent.builder()
                .app(this)
                .build()
                .inject(this)

    }
}