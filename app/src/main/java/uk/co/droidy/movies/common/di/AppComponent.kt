package uk.co.droidy.movies.common.di

import android.content.Context
import androidx.room.Room
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.Provides
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import uk.co.droidy.movies.api.di.ApiModule
import uk.co.droidy.movies.common.App
import uk.co.droidy.movies.db.AppDatabase
import uk.co.droidy.movies.ui.home.di.HomeModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidInjectionModule::class,
    AppModule::class,
    HomeModule::class,
    ApiModule::class
])
interface AppComponent: AndroidInjector<App> {
    override fun inject(application: App)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun app(application: App): Builder

        fun build(): AppComponent
    }
}