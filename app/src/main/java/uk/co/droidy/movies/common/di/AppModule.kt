package uk.co.droidy.movies.common.di

import android.content.Context
import androidx.room.Room
import dagger.Binds
import dagger.Module
import dagger.Provides
import uk.co.droidy.movies.common.App
import uk.co.droidy.movies.db.AppDatabase
import javax.inject.Singleton

@Module
abstract class AppModule {
    @Binds
    abstract fun bindContext(app: App): Context

    companion object {
        @Provides
        @Singleton
        fun provideAppDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(context, AppDatabase::class.java, AppDatabase.APP_DB_NAME)
                .build()
        }
    }

}