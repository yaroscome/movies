package uk.co.droidy.movies.common.di

import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import uk.co.droidy.movies.common.factories.AppViewModelFactory

@Module
abstract class ViewModelInjectionModule {

    @Binds
    abstract fun bindViewModelFactory(factory: AppViewModelFactory): ViewModelProvider.Factory

}