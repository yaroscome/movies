package uk.co.droidy.movies.common.dispatchers

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class AppDispatcher @Inject constructor(){

    fun getIo(): CoroutineDispatcher {
        return Dispatchers.IO
    }

    fun getMain(): CoroutineDispatcher {
        return Dispatchers.Main
    }

}