package uk.co.droidy.movies.db

import androidx.room.Database
import androidx.room.RoomDatabase
import uk.co.droidy.movies.api.dtos.MovieDTO

@Database(entities = [MovieDTO::class], version = 1)
abstract class AppDatabase: RoomDatabase() {

    abstract fun getMovieDtoDao(): MovieDtoDao

    companion object {
        const val APP_DB_NAME = "app_db"
    }

}