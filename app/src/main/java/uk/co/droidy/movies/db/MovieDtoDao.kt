package uk.co.droidy.movies.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import uk.co.droidy.movies.api.dtos.MovieDTO

@Dao
interface MovieDtoDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertMovies(movies: List<MovieDTO>): Array<Long>

    @Query("SELECT * FROM MovieDTO")
    fun getAllMovies(): List<MovieDTO>

    @Query("DELETE FROM MovieDTO WHERE id in (:idList)")
    fun deleteExpired(idList: List<Int>)

    @Query("SELECT * FROM MovieDTO WHERE title LIKE :searchTerm")
    fun getMoviesBy(searchTerm: String): List<MovieDTO>
}
