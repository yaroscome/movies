package uk.co.droidy.movies.db.home

import uk.co.droidy.movies.api.dtos.MovieDTO
import uk.co.droidy.movies.db.AppDatabase
import javax.inject.Inject

class HomeDbRepository @Inject constructor(private val appDatabase: AppDatabase) {

    fun insertMovies(movies: List<MovieDTO>) {
        appDatabase.getMovieDtoDao().insertMovies(movies)
    }

    fun getMoviesBy(searchTerm: String): List<MovieDTO> {
        val movieList = appDatabase.getMovieDtoDao().getMoviesBy(searchTerm)

        val expiredIds = movieList.filter { movieDTO -> isNowTenMinutesFrom(movieDTO.creationTime) }
            .map { it.id.toInt() }

        appDatabase.getMovieDtoDao().deleteExpired(expiredIds) //todo move to another function

        return movieList.filter { movieDTO -> !isNowTenMinutesFrom(movieDTO.creationTime) }
    }

    private fun isNowTenMinutesFrom(creationTime: Long): Boolean {
        val now = System.currentTimeMillis()
        return now - 600_000 > creationTime
    }

}