package uk.co.droidy.movies.domain

data class MovieDomain(
    val id: Int,
    val title: String,
    val year: Short,
    val genre: String,
    val poster: String
)
