package uk.co.droidy.movies.ui.common.watchers

import android.text.TextWatcher

interface AfterTextChangedWatcher: TextWatcher {
    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
}