package uk.co.droidy.movies.ui.home

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_home.*
import uk.co.droidy.movies.R
import uk.co.droidy.movies.databinding.ActivityHomeBinding
import uk.co.droidy.movies.ui.home.lists.MoviesAdapter
import javax.inject.Inject

class HomeActivity : AppCompatActivity() {

    @Inject
    lateinit var homeViewModel: HomeViewModel
    lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)
        initView()
    }

    private fun initView() {
        home_rv.layoutManager = GridLayoutManager(this, 2)
        home_rv.adapter = MoviesAdapter()

        homeViewModel.getMoviesLiveData().observe(this, Observer { state ->
            binding.viewState = state
        })
    }
}