package uk.co.droidy.movies.ui.home

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import uk.co.droidy.movies.common.dispatchers.AppDispatcher
import uk.co.droidy.movies.ui.home.binding.FailedState
import uk.co.droidy.movies.ui.home.binding.HomeViewState
import uk.co.droidy.movies.ui.home.binding.LoadingState
import uk.co.droidy.movies.ui.home.binding.SuccessState
import uk.co.droidy.movies.ui.home.lists.MovieModel
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val moviesUseCase: MoviesUseCase,
    private val appDispatcher: AppDispatcher
) : ViewModel(), HomeViewState.Actions {

    var viewState: HomeViewState = LoadingState(actions = this)
    val movieLiveData = MutableLiveData<HomeViewState>()

    fun getMoviesLiveData(): MutableLiveData<HomeViewState> {
        movieLiveData.value = viewState
        updateViewState()
        return movieLiveData
    }

    override fun onSearchTermChanged(searchTerm: String) {
        if (viewState.searchTerm == searchTerm) return
        movieLiveData.value = LoadingState(searchTerm, this)
        updateViewState(searchTerm)
        Log.i("Y__", "term:$searchTerm")
    }

    //TODO delay for a second and gather entire sequence
    private fun updateViewState(searchTerm: String = "") {
        CoroutineScope(appDispatcher.getIo()).launch {
            viewState = try {
                val movies = moviesUseCase.getMoviesBy(searchTerm).map {
                    MovieModel(it.title, it.year, it.poster)
                }

                SuccessState(movies, searchTerm,this@HomeViewModel)

            } catch (e: Exception) {
                val errorMessage: String =
                    e.message ?: "Generic error" //TODO change to Resource String
                FailedState(errorMessage, searchTerm, this@HomeViewModel)
            }

            withContext(appDispatcher.getMain()) {
                movieLiveData.value = viewState
            }
        }
    }
}