package uk.co.droidy.movies.ui.home

import uk.co.droidy.movies.api.home.HomeApiRepository
import uk.co.droidy.movies.db.home.HomeDbRepository
import uk.co.droidy.movies.domain.MovieDomain
import uk.co.droidy.movies.ui.home.mappers.MovieDtoDomainMapper
import javax.inject.Inject

class MoviesUseCase @Inject constructor(
    private val movieDtoDomainMapper: MovieDtoDomainMapper,
    private val homeApiRepository: HomeApiRepository,
    private val homeDbRepository: HomeDbRepository
) {
    suspend fun getMoviesBy(searchTerm: String): List<MovieDomain> {
        var filteredMovies = homeDbRepository.getMoviesBy(searchTerm)

        if (filteredMovies.isEmpty()) {
            val movies = homeApiRepository.getHomePage().data
            homeDbRepository.insertMovies(movies)

            filteredMovies = movies.filter { it.title.contains(searchTerm) }
        }

        return filteredMovies.map(movieDtoDomainMapper)
    }
}
