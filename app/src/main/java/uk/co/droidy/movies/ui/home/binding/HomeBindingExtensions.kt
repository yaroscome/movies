package uk.co.droidy.movies.ui.home.binding

import android.text.Editable
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.EditText
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.recyclerview.widget.RecyclerView
import uk.co.droidy.movies.ui.common.watchers.AfterTextChangedWatcher
import uk.co.droidy.movies.ui.home.lists.MoviesAdapter

@BindingAdapter("onLoading")
fun ProgressBar.onLoading(state: HomeViewState) {
    visibility = if (state is LoadingState) VISIBLE else GONE
}

@BindingAdapter("onErrorMessage")
fun TextView.onErrorMessage(state: HomeViewState) {
    visibility = if (state is FailedState) VISIBLE else GONE
}

@BindingAdapter("onErrorImage")
fun ImageView.onErrorImage(state: HomeViewState) {
    visibility = if (state is FailedState) VISIBLE else GONE
}

@BindingAdapter("onSuccessSearch")
fun EditText.onSuccessSearch(state: HomeViewState) {
    visibility = if (state is SuccessState) VISIBLE else GONE
}

@InverseBindingAdapter(attribute = "android:text", event = "android:textAttrChanged")
fun EditText.captureTextValue(): String {
    return text.toString()
}

@BindingAdapter("setOnTextChangedAction")
fun EditText.setOnTextChangedAction(action: HomeViewState.Actions) =
    this.addTextChangedListener(object : AfterTextChangedWatcher {
        override fun afterTextChanged(s: Editable?) {
            action.onSearchTermChanged(s.toString())
        }
    })

@BindingAdapter("onSuccessData")
fun RecyclerView.onSuccessData(state: HomeViewState) {
    val adapter = adapter as MoviesAdapter
    if (state is SuccessState) {
        adapter.data = state.data
        this.visibility = VISIBLE
    } else {
        adapter.data = emptyList()
        this.visibility = GONE
    }
}
