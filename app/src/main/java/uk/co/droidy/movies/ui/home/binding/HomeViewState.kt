package uk.co.droidy.movies.ui.home.binding

import uk.co.droidy.movies.ui.home.lists.MovieModel

sealed class HomeViewState(
    open val searchTerm: String = "",
    open val actions: Actions
) {
    interface Actions {
        fun onSearchTermChanged(searchTerm: String)
    }
}

data class LoadingState(
    override val searchTerm: String = "",
    override val actions: Actions
    ) : HomeViewState(searchTerm, actions)

data class FailedState(
    val message: String,
    override val searchTerm: String = "",
    override val actions: Actions
) : HomeViewState(searchTerm, actions)

data class SuccessState(
    val data: List<MovieModel>,
    override val searchTerm: String = "",
    override val actions: Actions
) : HomeViewState(searchTerm, actions)

