package uk.co.droidy.movies.ui.home.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap
import uk.co.droidy.movies.ui.home.HomeActivity
import uk.co.droidy.movies.ui.home.HomeViewModel
import kotlin.reflect.KClass

@Module
interface HomeModule {

    @ContributesAndroidInjector
    fun contributeHomeActivity(): HomeActivity

    @Binds
    @IntoMap
    @HomeViewModelKey(HomeViewModel::class)
    fun bindsHomeViewModel(homeViewModel: HomeViewModel): ViewModel
}

@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER
)
@MapKey
annotation class HomeViewModelKey(val value: KClass<HomeViewModel>)

