package uk.co.droidy.movies.ui.home.lists


data class MovieModel(
    val title: String,
    val year: Short,
    val poster: String
)