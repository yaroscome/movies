package uk.co.droidy.movies.ui.home.lists

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.movie_item.view.*
import uk.co.droidy.movies.R
import uk.co.droidy.movies.databinding.MovieItemBinding

class MovieViewHolder(private val binding: MovieItemBinding): RecyclerView.ViewHolder(binding.root) {
    fun bind(movieModel: MovieModel, listener: View.OnClickListener?) {
        binding.movieModel = movieModel
        binding.executePendingBindings()
    }

}
