package uk.co.droidy.movies.ui.home.lists

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import uk.co.droidy.movies.R
import uk.co.droidy.movies.databinding.MovieItemBinding

class MoviesAdapter: RecyclerView.Adapter<MovieViewHolder>() {

    var data: List<MovieModel> = emptyList()
    set(value) {
        field = value
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder =
        MovieViewHolder(
            MovieItemBinding.inflate(
                LayoutInflater.from(parent.context)
            )
        )

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bind(data[position], null)

    }

    override fun getItemCount(): Int = data.size


    companion object {
        @JvmStatic
        @BindingAdapter("poster")
        fun poster(poster_iv: ImageView, url: String) {
            Glide.with(poster_iv)
                .load(url)
                .placeholder(R.drawable.placeholder)
                .into(poster_iv)
        }
    }
}
