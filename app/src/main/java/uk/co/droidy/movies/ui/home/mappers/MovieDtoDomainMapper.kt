package uk.co.droidy.movies.ui.home.mappers

import uk.co.droidy.movies.api.dtos.MovieDTO
import uk.co.droidy.movies.domain.MovieDomain
import javax.inject.Inject

class MovieDtoDomainMapper @Inject constructor() : (MovieDTO) -> MovieDomain {
    override fun invoke(movieDto: MovieDTO): MovieDomain {
        return MovieDomain(
            movieDto.id.toInt(),
            movieDto.title,
            movieDto.year.toShort(),
            movieDto.genre,
            movieDto.poster
        )
    }
}