package uk.co.droidy.movies.ui.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.flextrade.jfixture.FixtureAnnotations
import com.flextrade.jfixture.annotations.Fixture
import com.jraska.livedata.test
import com.nhaarman.mockito_kotlin.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import uk.co.droidy.movies.common.dispatchers.AppDispatcher
import uk.co.droidy.movies.domain.MovieDomain
import uk.co.droidy.movies.ui.home.binding.HomeViewState
import uk.co.droidy.movies.ui.home.binding.SuccessState
import uk.co.droidy.movies.ui.home.lists.MovieModel

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class HomeViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Fixture
    lateinit var fixtMovieDomains: List<MovieDomain>

    @Mock lateinit var appDispatcher: AppDispatcher
    @Mock lateinit var mockMoviesUseCase: MoviesUseCase
    var mockActivity: Observer<HomeViewState> = mock()

    lateinit var sut: HomeViewModel

    @Before
    fun setUp() {
        FixtureAnnotations.initFixtures(this)
        sut = HomeViewModel(mockMoviesUseCase, appDispatcher)
        sut.movieLiveData.observeForever(mockActivity)
    }

    @Test
    fun `fetch and display movies success`() = runBlocking {
        whenever(mockMoviesUseCase.getMoviesBy("")).thenReturn(fixtMovieDomains)
        whenever(appDispatcher.getIo()).thenReturn(Dispatchers.Unconfined)
        whenever(appDispatcher.getMain()).thenReturn(Dispatchers.Unconfined)
        val fixtMovieModels = getModelFromDomain(fixtMovieDomains)

        val expectedState = SuccessState(fixtMovieModels, "", sut)

        sut.getMoviesLiveData()
            .test()
            .awaitValue()
            .assertValue(expectedState)

        Unit
    }
    
    private fun getModelFromDomain(domains: List<MovieDomain>): List<MovieModel> {
        val models = mutableListOf<MovieModel>()
        for (d in domains) {
            models.add(MovieModel(d.title, d.year, d.poster))
        }
        return models
    }
}

inline fun <reified T> mock(): T = Mockito.mock(T::class.java)