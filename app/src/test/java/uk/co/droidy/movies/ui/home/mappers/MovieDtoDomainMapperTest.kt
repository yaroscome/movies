package uk.co.droidy.movies.ui.home.mappers

import com.flextrade.jfixture.FixtureAnnotations
import com.flextrade.jfixture.annotations.Fixture
import junit.framework.TestCase.assertEquals
import org.junit.Before
import org.junit.Test
import uk.co.droidy.movies.api.dtos.MovieDTO

class MovieDtoDomainMapperTest {

    @Fixture lateinit var fixtMovieDTO: MovieDTO

    lateinit var sut: MovieDtoDomainMapper

    @Before
    fun setUp() {
        sut = MovieDtoDomainMapper()
    }

    @Test
    fun `verify mapping equals to expected`() {
        //setup
        FixtureAnnotations.initFixtures(this)
        fixtMovieDTO = fixtMovieDTO.copy(id = "1", year = "2021")

        //run
        val actual = sut.invoke(fixtMovieDTO)

        //verify
        assertEquals(fixtMovieDTO.id.toInt(), actual.id)
        assertEquals(fixtMovieDTO.title, actual.title)
        assertEquals(fixtMovieDTO.year.toShort(), actual.year)
        assertEquals(fixtMovieDTO.genre, actual.genre)
        assertEquals(fixtMovieDTO.poster, actual.poster)
    }

}